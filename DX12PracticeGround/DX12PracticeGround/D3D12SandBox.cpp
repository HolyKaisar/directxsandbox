#include "stdafx.h"
#include "D3D12SandBox.h"

D3D12SandBox::D3D12SandBox(UINT width, UINT height, std::wstring name) :
	DXAppFrameWork(width, height, name),
	m_uiFrameIndex(0),
	m_oViewport(),
	m_oScissorRect(),
	m_uiRTVDescriptorSize(0)
{
	m_oViewport.Width = static_cast<float>(width);
	m_oViewport.Height = static_cast<float>(height);
	m_oViewport.MaxDepth = 1.0f;

	m_oScissorRect.right = static_cast<LONG>(width);
	m_oScissorRect.bottom = static_cast<LONG>(height);
}

//D3D12SandBox::~D3D12SandBox() {
//	
//}

void D3D12SandBox::OnInit() {
	LoadPipeline();
	LoadAssets();
}

// Load the rendering pipeline dependencies
void D3D12SandBox::LoadPipeline() {
#if defined(_DEBUG)
	// Enable the D3D12 debug layer.
{
	ComPtr<ID3D12Debug> pDebugController;
	if (SUCCEEDED(D3D12GetDebugInterface(IID_PPV_ARGS(&pDebugController)))) pDebugController->EnableDebugLayer();
}
#endif

	ComPtr<IDXGIFactory4> factory;
	if (FAILED(CreateDXGIFactory1(IID_PPV_ARGS(&factory)))) throw;

	if (m_bUseWarpDevice) {
		ComPtr<IDXGIAdapter> warpAdapter;
		if (FAILED(factory->EnumWarpAdapter(IID_PPV_ARGS(&warpAdapter)))) throw;

		if (FAILED(D3D12CreateDevice(
			warpAdapter.Get(),
			D3D_FEATURE_LEVEL_11_0,
			IID_PPV_ARGS(&m_pDevice)))) throw;
	}
	else {
		ComPtr<IDXGIAdapter1> hardwareAdapter;
		GetHardwareAdapter(factory.Get(), &hardwareAdapter);

		if (FAILED(D3D12CreateDevice(
			hardwareAdapter.Get(),
			D3D_FEATURE_LEVEL_11_0,
			IID_PPV_ARGS(&m_pDevice)))) throw;
	}

	// Describe and create the command queue.
	D3D12_COMMAND_QUEUE_DESC queueDesc = {};
	queueDesc.Flags = D3D12_COMMAND_QUEUE_FLAG_NONE;
	queueDesc.Type = D3D12_COMMAND_LIST_TYPE_DIRECT;

	if (FAILED(m_pDevice->CreateCommandQueue(&queueDesc, IID_PPV_ARGS(&m_pCommandQueue)))) throw;

	// Describe and create the swap chain.
	DXGI_SWAP_CHAIN_DESC1 swapChainDesc = {};
	swapChainDesc.BufferCount = FrameCount;
	swapChainDesc.Width = m_uiWidth;
	swapChainDesc.Height = m_uiHeight;
	swapChainDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_FLIP_DISCARD;
	swapChainDesc.SampleDesc.Count = 1;

	ComPtr<IDXGISwapChain1> swapChain;
	if (FAILED(factory->CreateSwapChainForHwnd(
		m_pCommandQueue.Get(),  // Swap chain needs the queue so that it can force a flush on it
		Win32Application::GetHwnd(),
		&swapChainDesc,
		nullptr,
		nullptr,
		&swapChain))) throw;

		// No Support fullscreen
		if (FAILED(factory->MakeWindowAssociation(Win32Application::GetHwnd(), DXGI_MWA_NO_ALT_ENTER))) throw;

		if (FAILED(swapChain.As(&m_pSwapChain))) throw;
		m_uiFrameIndex = m_pSwapChain->GetCurrentBackBufferIndex();

	// Create descriptor heaps.
	{
		// Describe and create a render target view (RTV) desciptor heap.
		D3D12_DESCRIPTOR_HEAP_DESC rtvHeapDesc = {};
		rtvHeapDesc.NumDescriptors = FrameCount;
		rtvHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_RTV;
		rtvHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
		if (FAILED(m_pDevice->CreateDescriptorHeap(&rtvHeapDesc, IID_PPV_ARGS(&m_pRTVHeap)))) throw;

		m_uiRTVDescriptorSize = m_pDevice->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_RTV);

		// Describe and create a constant buffer view (CBV) descriptor heap.
		// Flags indicate that this descriptor heap can be bound to the pipeline 
		// and that descriptors contained in it can be referenced by a root table.
		D3D12_DESCRIPTOR_HEAP_DESC cbvHeapDesc = {};
		cbvHeapDesc.NumDescriptors = 1;
		cbvHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
		cbvHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
		ThrowIfFailed(m_pDevice->CreateDescriptorHeap(&cbvHeapDesc, IID_PPV_ARGS(&m_pCbvHeap)));

		// Describe and create a depth stencil view descriptor heap.
		D3D12_DESCRIPTOR_HEAP_DESC dsvHeapDesc = {};
		dsvHeapDesc.NumDescriptors = 1;
		dsvHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
		dsvHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_DSV;
		ThrowIfFailed(m_pDevice->CreateDescriptorHeap(&dsvHeapDesc, IID_PPV_ARGS(&m_pDSVHeap)));
	}

	// Create frame resources.
	{
		CD3DX12_CPU_DESCRIPTOR_HANDLE rtvHandle(m_pRTVHeap->GetCPUDescriptorHandleForHeapStart());

		// Create a RTV for each frame.
		for (UINT n = 0; n < FrameCount; n++) {
			if (FAILED(m_pSwapChain->GetBuffer(n, IID_PPV_ARGS(&m_pRenderTargets[n])))) throw;
			m_pDevice->CreateRenderTargetView(m_pRenderTargets[n].Get(), nullptr, rtvHandle);
			rtvHandle.Offset(1, m_uiRTVDescriptorSize);

			if (FAILED(m_pDevice->CreateCommandAllocator(D3D12_COMMAND_LIST_TYPE_DIRECT, IID_PPV_ARGS(&m_pCommandAllocator[n])))) throw;
		}
	}
}

void D3D12SandBox::LoadAssets() {
	// Create a root signature consisting of a descriptor table with a single CBV.
	{
		CD3DX12_DESCRIPTOR_RANGE range[1];
		CD3DX12_ROOT_PARAMETER rootParameters[1];

		range[0].Init(D3D12_DESCRIPTOR_RANGE_TYPE_CBV, 1, 0);
		rootParameters[0].InitAsDescriptorTable(1, &range[0], D3D12_SHADER_VISIBILITY_VERTEX);

		// Allow input layout and deny uneccessary access to certain pipeline stages.
		D3D12_ROOT_SIGNATURE_FLAGS rootSignatureFlags =
			D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT |
			D3D12_ROOT_SIGNATURE_FLAG_DENY_HULL_SHADER_ROOT_ACCESS |
			D3D12_ROOT_SIGNATURE_FLAG_DENY_DOMAIN_SHADER_ROOT_ACCESS |
			D3D12_ROOT_SIGNATURE_FLAG_DENY_GEOMETRY_SHADER_ROOT_ACCESS |
			D3D12_ROOT_SIGNATURE_FLAG_DENY_PIXEL_SHADER_ROOT_ACCESS;


		CD3DX12_ROOT_SIGNATURE_DESC rootSignatureDesc;
		rootSignatureDesc.Init(_countof(rootParameters), rootParameters, 0, nullptr, rootSignatureFlags);

		ComPtr<ID3DBlob> signature;
		ComPtr<ID3DBlob> error;
		// TODO why we need this D3D12SerializeRootSignature, why we need to serialize form to pass to create root signature
		if (FAILED(D3D12SerializeRootSignature(&rootSignatureDesc, D3D_ROOT_SIGNATURE_VERSION_1, &signature, &error))) throw;
		if (FAILED(m_pDevice->CreateRootSignature(0, signature->GetBufferPointer(), signature->GetBufferSize(), IID_PPV_ARGS(&m_pRootSignature)))) throw;
	}

	// Create the pipeline state, which includes compiling and loading shaders.
	{
		ComPtr<ID3DBlob> vertexShader;
		ComPtr<ID3DBlob> pixelShader;

#if defined(_DEBUG)
		// Enable better shader debugging with the graphics debugging tools.
		UINT compileFlags = D3DCOMPILE_DEBUG | D3DCOMPILE_SKIP_OPTIMIZATION;
#else
		UINT  compileFlags = 0;
#endif
		// TODO need to write a function to handle all return errors later
		// Some error such as wrong hlsl file name cannot be capture at this time, need to get the hr from D3DCompileFromFile 
		// and perform further analysis
		ComPtr<ID3DBlob> pErrorBlob;
		if (FAILED(D3DCompileFromFile(/*GetAssetFullPath(L"Cube.hlsl").c_str()*/L"Cube.hlsl", nullptr, nullptr, "VSMain", "vs_5_0", compileFlags, 0, &vertexShader, &pErrorBlob))) {
			if (pErrorBlob) {
				OutputDebugStringA(reinterpret_cast<const char*>(pErrorBlob->GetBufferPointer()));
				pErrorBlob = nullptr;
			}
			throw;
		}
		if (FAILED(D3DCompileFromFile(/*GetAssetFullPath(L"Cube.hlsl").c_str()*/L"Cube.hlsl", nullptr, nullptr, "PSMain", "ps_5_0", compileFlags, 0, &pixelShader, &pErrorBlob))) {
			if (pErrorBlob) {
				OutputDebugStringA(reinterpret_cast<const char*>(pErrorBlob->GetBufferPointer()));
				pErrorBlob = nullptr;
			}
			throw;
		}

		// Define the vertex input layout.
		D3D12_INPUT_ELEMENT_DESC inputElementDescs[] =
		{
			{"POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0},
			{"COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 12, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0},
		};

		D3D12_RASTERIZER_DESC rsDesc = {};
		rsDesc.FillMode = D3D12_FILL_MODE_SOLID;
		rsDesc.CullMode = D3D12_CULL_MODE_NONE;
		rsDesc.FrontCounterClockwise = false;
		rsDesc.DepthBias = false;
		rsDesc.DepthBiasClamp = 0;
		rsDesc.SlopeScaledDepthBias = 0;
		rsDesc.DepthClipEnable = true;
		rsDesc.MultisampleEnable = false;
		rsDesc.AntialiasedLineEnable = false;
		rsDesc.ForcedSampleCount = 0;

		// Describe and create the graphics pipeline state objects
		D3D12_GRAPHICS_PIPELINE_STATE_DESC psoDesc = {};
		psoDesc.InputLayout = { inputElementDescs, _countof(inputElementDescs) };
		psoDesc.pRootSignature = m_pRootSignature.Get();
		psoDesc.VS = CD3DX12_SHADER_BYTECODE(vertexShader.Get());
		psoDesc.PS = CD3DX12_SHADER_BYTECODE(pixelShader.Get());
		//psoDesc.RasterizerState = CD3DX12_RASTERIZER_DESC(D3D12_DEFAULT);
		psoDesc.RasterizerState = CD3DX12_RASTERIZER_DESC(rsDesc);
		psoDesc.BlendState = CD3DX12_BLEND_DESC(D3D12_DEFAULT);
		psoDesc.DepthStencilState.DepthEnable = TRUE;
		psoDesc.DSVFormat = DXGI_FORMAT_D32_FLOAT;
		psoDesc.DepthStencilState.DepthFunc = D3D12_COMPARISON_FUNC_GREATER;
		psoDesc.DepthStencilState.DepthWriteMask = D3D12_DEPTH_WRITE_MASK_ALL;
		// Stencil operations if pixel is front-facing
		psoDesc.DepthStencilState.FrontFace.StencilFailOp = D3D12_STENCIL_OP_KEEP;
		psoDesc.DepthStencilState.FrontFace.StencilDepthFailOp = D3D12_STENCIL_OP_INCR;
		psoDesc.DepthStencilState.FrontFace.StencilPassOp = D3D12_STENCIL_OP_KEEP;
		psoDesc.DepthStencilState.FrontFace.StencilFunc = D3D12_COMPARISON_FUNC_ALWAYS;
		// Stencil operations if pixel is back-facing
		psoDesc.DepthStencilState.BackFace.StencilFailOp = D3D12_STENCIL_OP_KEEP;
		psoDesc.DepthStencilState.BackFace.StencilDepthFailOp = D3D12_STENCIL_OP_DECR;
		psoDesc.DepthStencilState.BackFace.StencilPassOp = D3D12_STENCIL_OP_KEEP;
		psoDesc.DepthStencilState.BackFace.StencilFunc = D3D12_COMPARISON_FUNC_ALWAYS;
		psoDesc.DepthStencilState.StencilEnable = FALSE;
		psoDesc.SampleMask = 0xffffffff;
		psoDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
		psoDesc.NumRenderTargets = 1;
		psoDesc.RTVFormats[0] = DXGI_FORMAT_R8G8B8A8_UNORM;
		psoDesc.SampleDesc.Count = 1;
		if (FAILED(m_pDevice->CreateGraphicsPipelineState(&psoDesc, IID_PPV_ARGS(&m_pPipelineState)))) throw;
	}

	// Create the command list
	if (FAILED(m_pDevice->CreateCommandList(0, D3D12_COMMAND_LIST_TYPE_DIRECT, m_pCommandAllocator[m_uiFrameIndex].Get(), m_pPipelineState.Get(), IID_PPV_ARGS(&m_pCommandList)))) throw;

	// The command list is in the recording state, close it for now
	if (FAILED(m_pCommandList->Close())) throw;

	// Create the vertex buffer
	{
		Vertex3D triangleVertices[] =
		{
			{ {-1.0f, 1.0f, -1.0f}, {1.0f, 0.0f, 0.0f, 0.0f} },
			{ {1.0f, 1.0f, -1.0f}, { 0.0f, 1.0f, 0.0f, 0.0f } },
			{ {1.0f, 1.0f, 1.0f}, { 0.0f, 0.0f, 1.0f, 0.0f } },
			{ {-1.0f, 1.0f, 1.0f}, { 1.0f, 0.0f, 1.0f, 0.0f } },
			  
			{ {-1.0f, -1.0f, -1.0f}, { 0.0f, 1.0f, 0.0f, 0.0f } },
			{ {1.0f, -1.0f, -1.0f}, { 1.0f, 0.0f, 0.0f, 0.0f } },
			{ {1.0f, -1.0f, 1.0f}, { 1.0f, 1.0f, 0.0f, 0.0f } },
			{ {-1.0f, -1.0f, 1.0f}, { 0.0f, 0.0f, 1.0f, 0.0f } },
			  
			{ {-1.0f, -1.0f, 1.0f}, { 0.0f, 0.0f, 1.0f, 0.0f } },
			{ {-1.0f, -1.0f, -1.0f}, { 1.0f, 0.0f, 0.0f, 0.0f } },
			{ {-1.0f, 1.0f, -1.0f}, { 0.0f, 1.0f, 0.0f, 0.0f } },
			{ {-1.0f, 1.0f, 1.0f}, { 1.0f, 1.0f, 1.0f, 0.0f } },
			  
			{ {1.0f, -1.0f, 1.0f}, { 0.0f, 1.0f, 1.0f, 0.0f } },
			{ {1.0f, -1.0f, -1.0f}, { 1.0f, 0.3f, 0.0f, 0.0f } },
			{ {1.0f, 1.0f, -1.0f}, { 0.0f, 0.0f, 1.0f, 0.0f } },
			{ {1.0f, 1.0f, 1.0f}, { 0.5f, 1.0f, 0.5f, 0.0f } },
			  
			{ {-1.0f, -1.0f, -1.0f}, { 0.2f, 1.0f, 0.0f, 0.0f } },
			{ {1.0f, -1.0f, -1.0f}, { 0.0f, 1.0f, 0.3f, 0.0f } },
			{ {1.0f, 1.0f, -1.0f}, { 0.5f, 1.0f, 0.2f, 0.0f } },
			{ {-1.0f, 1.0f, -1.0f}, { 0.1f, 0.5f, 1.0f, 0.0f } },
			  
			{ {-1.0f, -1.0f, 1.0f}, { 1.0f, 0.1f, 1.0f, 0.0f } },
			{ {1.0f, -1.0f, 1.0f}, { 0.3f, 1.0f, 1.0f, 0.0f } },
			{ {1.0f, 1.0f, 1.0f}, { 0.2f, 0.0f, 0.6f, 0.0f } },
			{ {-1.0f, 1.0f, 1.0f}, { 0.0f, 0.0f, 1.0f, 0.0f } }
		};

		const UINT vertexBufferSize = sizeof(triangleVertices);

		// TODO upload heap is very inefficient here
		// TODO figure out the d3d12_heap_type_upload here
		if (FAILED(m_pDevice->CreateCommittedResource(
			&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD),
			D3D12_HEAP_FLAG_NONE,
			&CD3DX12_RESOURCE_DESC::Buffer(vertexBufferSize),
			D3D12_RESOURCE_STATE_GENERIC_READ,
			nullptr,
			IID_PPV_ARGS(&m_pVertexBuffer)
			))) throw;

		// Copy the triangle data to the vertex buffer.
		UINT8* pVertexDataBegin;
		CD3DX12_RANGE readRange(0, 0); // Do not intend to read from this resource
		if (FAILED(m_pVertexBuffer->Map(0, &readRange, reinterpret_cast<void**>(&pVertexDataBegin)))) throw;
		memcpy(pVertexDataBegin, triangleVertices, sizeof(triangleVertices));
		m_pVertexBuffer->Unmap(0, nullptr);

		// Initialize the vertex buffer view.
		m_oVertexBufferView.BufferLocation = m_pVertexBuffer->GetGPUVirtualAddress();
		m_oVertexBufferView.StrideInBytes = sizeof(Vertex3D);
		m_oVertexBufferView.SizeInBytes = vertexBufferSize;
	}

	// Create the index buffer
	{
		unsigned int triangleIndex[] = 
		{
			3,1,0,
			2,1,3,

			6,4,5,
			7,4,6,

			11,9,8,
			10,9,11,

			14,12,13,
			15,12,14,

			19,17,16,
			18,17,19,

			22,20,21,
			23,20,22
		};

		const UINT indexBufferSize = sizeof(triangleIndex);

		if (FAILED(m_pDevice->CreateCommittedResource(
			&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD),
			D3D12_HEAP_FLAG_NONE,
			&CD3DX12_RESOURCE_DESC::Buffer(indexBufferSize),
			D3D12_RESOURCE_STATE_GENERIC_READ,
			nullptr,
			IID_PPV_ARGS(&m_pIndexBuffer)
			))) throw;

		// Copy the triangle data to the index buffer.
		UINT8* pIndexDataBegin;
		CD3DX12_RANGE readRange(0, 0);
		if (FAILED(m_pIndexBuffer->Map(0, &readRange, reinterpret_cast<void**>(&pIndexDataBegin)))) throw;
		memcpy(pIndexDataBegin, triangleIndex, sizeof(triangleIndex));
		m_pIndexBuffer->Unmap(0, nullptr);

		// Initialize the index buffer view.
		m_oIndexBufferView.BufferLocation = m_pIndexBuffer->GetGPUVirtualAddress();
		m_oIndexBufferView.Format = DXGI_FORMAT_R32_UINT;
		m_oIndexBufferView.SizeInBytes = indexBufferSize;
	}

	// Create the constant buffer
	{
		if (FAILED(m_pDevice->CreateCommittedResource(
			&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD),
			D3D12_HEAP_FLAG_NONE,
			&CD3DX12_RESOURCE_DESC::Buffer(1024 * 64), // TODO why this is 1024 * 64? why is 1024
			D3D12_RESOURCE_STATE_GENERIC_READ,
			nullptr,
			IID_PPV_ARGS(&m_pConstantBuffer)))) throw;

		D3D12_CONSTANT_BUFFER_VIEW_DESC cbvDesc = {};
		cbvDesc.BufferLocation = m_pConstantBuffer->GetGPUVirtualAddress();
		// !!! Just remember computers use complement code, so, ~00111 = 11000 
		//cbvDesc.SizeInBytes = (sizeof(CBuffer) + 255) & ~255; // CB size is required to be 256-byte aligned
		cbvDesc.SizeInBytes = AlignTo256Byte(sizeof(CBuffer)); // CB size is required to be 256-byte aligned
		m_pDevice->CreateConstantBufferView(&cbvDesc, m_pCbvHeap->GetCPUDescriptorHandleForHeapStart());

		// Initialize and map the constant buffers. We don't unmap this until the
		// app closes. Keeping things mapped for the lifetime of the resource is okay.
		ZeroMemory(&m_oConstantBufferData, sizeof(m_oConstantBufferData));

		CD3DX12_RANGE readRange(0, 0);		// We do not intend to read from this resource on the CPU.
		ThrowIfFailed(m_pConstantBuffer->Map(0, &readRange, reinterpret_cast<void**>(&m_pCbvDataBegin)));
		memcpy(m_pCbvDataBegin, &m_oConstantBufferData, sizeof(m_oConstantBufferData));
	}

	// Create depth stencil view
	{
		CD3DX12_RESOURCE_DESC depthStencilDesc(D3D12_RESOURCE_DIMENSION_TEXTURE2D, 0, 1280, 720, 
			1, 1, DXGI_FORMAT_D32_FLOAT, 1, 0, D3D12_TEXTURE_LAYOUT_UNKNOWN,
			D3D12_RESOURCE_FLAG_ALLOW_DEPTH_STENCIL | D3D12_RESOURCE_FLAG_DENY_SHADER_RESOURCE);

		D3D12_CLEAR_VALUE clearValue;
		clearValue.Format = DXGI_FORMAT_D32_FLOAT;
		clearValue.DepthStencil.Depth = 0.0f;
		clearValue.DepthStencil.Stencil = 0;


		if (FAILED(m_pDevice->CreateCommittedResource(
			&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT),
			D3D12_HEAP_FLAG_NONE,
			&depthStencilDesc, // TODO why this is 1024 * 64? why is 1024
			D3D12_RESOURCE_STATE_DEPTH_WRITE,
			&clearValue,
			IID_PPV_ARGS(&m_pDepthStencil)))) throw;

		D3D12_DEPTH_STENCIL_VIEW_DESC depthStencilViewDesc = {};
		depthStencilViewDesc.Format = DXGI_FORMAT_D32_FLOAT;
		depthStencilViewDesc.ViewDimension = D3D12_DSV_DIMENSION_TEXTURE2D;
		depthStencilViewDesc.Texture2D.MipSlice = 0;
		m_pDevice->CreateDepthStencilView(m_pDepthStencil.Get(), nullptr, m_pDSVHeap->GetCPUDescriptorHandleForHeapStart());
	}

	// Create synchronization objects and wait until assets have been uploaded to the GPU
	{
		if (FAILED(m_pDevice->CreateFence(m_u64FenceValue[m_uiFrameIndex], D3D12_FENCE_FLAG_NONE, IID_PPV_ARGS(&m_pFence)))) throw;
		m_u64FenceValue[m_uiFrameIndex]++;

		// Create an event handle to use for frame synchronization.
		m_fenceEvent = CreateEvent(nullptr, FALSE, FALSE, nullptr);
		if (m_fenceEvent = nullptr) {
			if (FAILED(HRESULT_FROM_WIN32(GetLastError()))) throw;
		}

		//WaitForPreviousFrame();
		WaitForGPU();
	}

	// Create the Camera
	{
#define SIM_ORBIT_RADIUS 10.f
#define SIM_DISC_RADIUS  1.f
#define SIM_MIN_SCALE    0.1f

		auto center = XMVectorSet(0.0f, -0.4f * SIM_DISC_RADIUS, 0.0f, 0.0f);
		auto radius = SIM_ORBIT_RADIUS + SIM_DISC_RADIUS + 2.f;
		auto minRadius = SIM_ORBIT_RADIUS - 8.0f * SIM_DISC_RADIUS;
		auto maxRadius = SIM_ORBIT_RADIUS + 8.0f * SIM_DISC_RADIUS;
		auto longAngle = 4.50f;
		auto latAngle = 1.45f;
		m_Camera.View(center, radius, minRadius, maxRadius, longAngle, latAngle);

		float aspect = 1280.0f / 720.0f;
		m_Camera.Projection(XM_PIDIV2 * 0.8f * 3 / 2, aspect);
	}
}

// Update frame-based values
void D3D12SandBox::OnUpdate() {
	const float translationSpeed = 0.01f;
	const float offsetBounds = 1.25f;

	m_oConstantBufferData.mOffset.x += translationSpeed;
	if (m_oConstantBufferData.mOffset.x > offsetBounds) m_oConstantBufferData.mOffset.x = -offsetBounds;

	static float angles = 0;
	angles += translationSpeed;
	float sine = sin(angles);
	float cosine = cos(angles);
	if (angles > 2 * XM_PI) angles = 0.0f;

	m_oConstantBufferData.mOffset.y = max(sine, 0.0f);
	m_oConstantBufferData.mOffset.z = max(cosine, 0.0f);
	m_oConstantBufferData.mOffset.w = sine * cosine;

	// Update Camera states
	m_oConstantBufferData.mViewProjMatrix = XMMatrixTranspose(m_Camera.ViewProjection());
	//m_oConstantBufferData.mViewProjMatrix = XMMatrixTranspose(view_proj);
	memcpy(m_pCbvDataBegin, &m_oConstantBufferData, sizeof(m_oConstantBufferData));
}

// Render the scene
void D3D12SandBox::OnRender() {
	// Record all the commands we need to render the scene into the command list.
	PopulateCommandList();

	// Execute the command list.
	ID3D12CommandList* ppCommandLists[] = { m_pCommandList.Get() };
	m_pCommandQueue->ExecuteCommandLists(_countof(ppCommandLists), ppCommandLists);

	// Present the frame.
	if (FAILED(m_pSwapChain->Present(1, 0))) throw;

	//WaitForPreviousFrame();
	//WaitForGPU();
	MoveToNextFrame();
}

void D3D12SandBox::OnDestroy() {
	//WaitForPreviousFrame();
	WaitForGPU();

	CloseHandle(m_fenceEvent);
}

void D3D12SandBox::PopulateCommandList() {
	// Command list allocators can only be reset when the associated
	// command lists have finished execution on the GPU; apps should use fences to determine GPU
	// execution progress.

	if (FAILED(m_pCommandAllocator[m_uiFrameIndex]->Reset())) throw;

	// Before recode the command list that is used by ExecuteCommandList(), you need to reset
	if (FAILED(m_pCommandList->Reset(m_pCommandAllocator[m_uiFrameIndex].Get(), m_pPipelineState.Get()))) throw;

	// Set necessary state.
	m_pCommandList->SetGraphicsRootSignature(m_pRootSignature.Get());

	ID3D12DescriptorHeap* ppHeaps[] = { m_pCbvHeap.Get() };
	m_pCommandList->SetDescriptorHeaps(_countof(ppHeaps), ppHeaps);

	m_pCommandList->SetGraphicsRootDescriptorTable(0, m_pCbvHeap->GetGPUDescriptorHandleForHeapStart());
	m_pCommandList->RSSetViewports(1, &m_oViewport);
	m_pCommandList->RSSetScissorRects(1, &m_oScissorRect);

	// Indicate that the back buffer will be used as a render target.
	m_pCommandList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(m_pRenderTargets[m_uiFrameIndex].Get(), D3D12_RESOURCE_STATE_PRESENT, D3D12_RESOURCE_STATE_RENDER_TARGET));

	CD3DX12_CPU_DESCRIPTOR_HANDLE rtvHandle(m_pRTVHeap->GetCPUDescriptorHandleForHeapStart(), m_uiFrameIndex, m_uiRTVDescriptorSize);
	CD3DX12_CPU_DESCRIPTOR_HANDLE dsvHandle(m_pDSVHeap->GetCPUDescriptorHandleForHeapStart());
	m_pCommandList->OMSetRenderTargets(1, &rtvHandle, FALSE, &dsvHandle);

	// Record commands.
	const float clearColor[] = { 0.0f, 0.2f, 0.4f, 1.0f };
	m_pCommandList->ClearRenderTargetView(rtvHandle, clearColor, 0, nullptr);
	m_pCommandList->ClearDepthStencilView(dsvHandle, D3D12_CLEAR_FLAG_DEPTH, 0.0, 0, 0, nullptr);
	m_pCommandList->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	m_pCommandList->IASetVertexBuffers(0, 1, &m_oVertexBufferView);
	m_pCommandList->IASetIndexBuffer(&m_oIndexBufferView);
	//m_pCommandList->DrawInstanced(3, 1, 0, 0);
	m_pCommandList->DrawIndexedInstanced(36, 1, 0, 0, 0);

	m_pCommandList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(m_pRenderTargets[m_uiFrameIndex].Get(), D3D12_RESOURCE_STATE_RENDER_TARGET, D3D12_RESOURCE_STATE_PRESENT));

	if (FAILED(m_pCommandList->Close())) throw;
}

// Deprecated
//void D3D12SandBox::WaitForPreviousFrame() {
//	// Wait for current frame to finish is very inefficient
//
//	// Signal and increment the fence value.
//	const UINT64 fence = m_u64FenceValue;
//	if (FAILED(m_pCommandQueue->Signal(m_pFence.Get(), fence))) throw;
//	m_u64FenceValue++;
//
//	// Wait until the previous frame is finished.
//	if (m_pFence->GetCompletedValue() < fence) {
//		if (FAILED(m_pFence->SetEventOnCompletion(fence, m_fenceEvent))) throw;
//		WaitForSingleObject(m_fenceEvent, INFINITE);
//	}
//
//	m_uiFrameIndex = m_pSwapChain->GetCurrentBackBufferIndex();
//}

void D3D12SandBox::WaitForGPU() {
	// Schedule a signal command in queue
	if (FAILED(m_pCommandQueue->Signal(m_pFence.Get(), m_u64FenceValue[m_uiFrameIndex]))) throw;

	// Wait until the fence has been processed
	if (FAILED(m_pFence->SetEventOnCompletion(m_u64FenceValue[m_uiFrameIndex], m_fenceEvent))) throw;
	WaitForSingleObjectEx(m_fenceEvent, INFINITE, FALSE);

	// Increment the fence value for the current frame
	m_u64FenceValue[m_uiFrameIndex]++;
}

void D3D12SandBox::MoveToNextFrame() {
	// Schedule a signal command in queue
	const UINT64 currentFenceValue = m_u64FenceValue[m_uiFrameIndex];
	if (FAILED(m_pCommandQueue->Signal(m_pFence.Get(), currentFenceValue))) throw;

	// Update the frame index
	m_uiFrameIndex = m_pSwapChain->GetCurrentBackBufferIndex();

	// If the next frame is not ready to be rendered yet, wait until it is ready,
	if (m_pFence->GetCompletedValue() < m_u64FenceValue[m_uiFrameIndex]) {
		if (FAILED(m_pFence->SetEventOnCompletion(m_u64FenceValue[m_uiFrameIndex], m_fenceEvent))) throw;
		WaitForSingleObjectEx(m_fenceEvent, INFINITE, FALSE);
	}

	// Set the fence value for the next frame
	m_u64FenceValue[m_uiFrameIndex] = currentFenceValue + 1;
}

LRESULT D3D12SandBox::HandleMessages(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
	switch (uMsg) {
	case WM_MOUSEWHEEL:
		{
			auto delta = GET_WHEEL_DELTA_WPARAM(wParam);
			m_Camera.ZoomRadius(-0.01f * delta);
			break;
		}
	case WM_POINTERDOWN:
	case WM_POINTERUPDATE:
	case WM_POINTERUP: 
		{
			auto pointerId = GET_POINTERID_WPARAM(wParam);
			POINTER_INFO pointerInfo;
			if (GetPointerInfo(pointerId, &pointerInfo)) {
				if (uMsg == WM_POINTERDOWN) {
					POINT p = pointerInfo.ptPixelLocation;
					ScreenToClient(hWnd, &p);
					RECT clientRect;
					GetClientRect(hWnd, &clientRect);
					p.x = p.x * 1280.0f / (clientRect.right - clientRect.left);
					p.y = p.y * 720.0f / (clientRect.bottom - clientRect.top);

					m_Camera.AddPointer(pointerId);
				}
			}

			m_Camera.ProcessPointerFrames(pointerId, &pointerInfo);
			if (uMsg == WM_POINTERUP) m_Camera.RemovePointer(pointerId);
			break;
		}
	}

	return 0;
}

void D3D12SandBox::AfterWinMsgProc() {
	// Camera Inertia feature
	m_Camera.ProcessInertia();
}

