#include "stdafx.h"
#include "D3D12SandBox.h"

_Use_decl_annotations_
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE, LPSTR, int nCmdShow)
{
	D3D12SandBox DXApp(1280, 720, L"D3D12 Sand Box");
	return Win32Application::Run(&DXApp, hInstance, nCmdShow);
}