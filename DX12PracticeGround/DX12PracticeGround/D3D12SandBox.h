#pragma once

#include "DXAppFrameWork.h"
#include "Camera.h"

using namespace DirectX;
using Microsoft::WRL::ComPtr;

class D3D12SandBox : public DXAppFrameWork
{
public:
	D3D12SandBox(UINT width, UINT height, std::wstring name);
	//~D3D12SandBox();

	virtual void OnInit();
	virtual void OnUpdate();
	virtual void OnRender();
	virtual void OnDestroy();

	LRESULT HandleMessages(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
	void AfterWinMsgProc();
	virtual OrbitCamera& GetCameraHandle() { return m_Camera; }

private:
	struct Vertex3D {
		XMFLOAT3 mPosition;
		XMFLOAT4 mColor;
	};

	struct CBuffer {
		XMFLOAT4 mOffset;
		//Matrix4  mViewProjMatrix;
		XMMATRIX  mViewProjMatrix;
		XMFLOAT3 mViewerPosition;
	};
private:
	static const UINT FrameCount = 2;

	/*
	* Pipeline State Objects
	*/
	D3D12_VIEWPORT m_oViewport;
	D3D12_RECT m_oScissorRect;

	ComPtr<IDXGISwapChain3> m_pSwapChain;
	ComPtr<ID3D12Device> m_pDevice;
	ComPtr<ID3D12Resource> m_pRenderTargets[FrameCount];
	ComPtr<ID3D12CommandAllocator> m_pCommandAllocator[FrameCount];
	ComPtr<ID3D12CommandQueue> m_pCommandQueue;
	ComPtr<ID3D12RootSignature> m_pRootSignature;
	ComPtr<ID3D12DescriptorHeap> m_pRTVHeap;
	ComPtr<ID3D12DescriptorHeap> m_pCbvHeap;
	ComPtr<ID3D12DescriptorHeap> m_pDSVHeap;
	ComPtr<ID3D12PipelineState> m_pPipelineState;
	ComPtr<ID3D12GraphicsCommandList> m_pCommandList;
	UINT m_uiRTVDescriptorSize;

	// App resources
	ComPtr<ID3D12Resource> m_pVertexBuffer;
	D3D12_VERTEX_BUFFER_VIEW m_oVertexBufferView;

	ComPtr<ID3D12Resource> m_pIndexBuffer;
	D3D12_INDEX_BUFFER_VIEW m_oIndexBufferView;

	ComPtr<ID3D12Resource> m_pConstantBuffer;
	CBuffer m_oConstantBufferData;
	UINT8* m_pCbvDataBegin;

	ComPtr<ID3D12Resource> m_pDepthStencil;

	OrbitCamera m_Camera;

	// Synchronization objects
	UINT m_uiFrameIndex;
	HANDLE m_fenceEvent;
	ComPtr<ID3D12Fence> m_pFence;
	UINT64 m_u64FenceValue[FrameCount];
	//UINT64 m_u64FenceValue; // Deprecated

	void LoadPipeline();
	void LoadAssets();
	void PopulateCommandList();
	//void WaitForPreviousFrame();
	
	void WaitForGPU();
	void MoveToNextFrame();
};

