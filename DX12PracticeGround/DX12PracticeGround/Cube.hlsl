
cbuffer ConstantBuffer : register(b0)
{
	float4	 offset;
	float4x4 viewProjMat;
	float3   viewerPos;
};

struct VSInput {
	float3 position : POSITION;
	float4 color : COLOR;
};

struct PSInput {
	float4 position : SV_POSITION;
	float4 color : COLOR;
};

PSInput VSMain(VSInput input) {
	//PSInput output = (PSInput)0;
	PSInput output;

	//output.position = mul(viewProjMat, float4(input.position, 1.0f));
	output.position = mul(float4(input.position, 1.0f), viewProjMat);
	//output.position = float4(input.position, 1.0f) + offset;
	output.color = input.color + float4(offset.yzw, 0.0f);

	return output;
}

float4 PSMain( PSInput input ) : SV_TARGET {
	return input.color;
}