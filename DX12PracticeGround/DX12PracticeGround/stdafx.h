
// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently.

#pragma once

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN  // Exclude rarely-used stuff from Windows headers.
#endif

// Windows header files
#include <Windows.h>
#include <string>
#include <wrl.h>
#include <shellapi.h>
#include <iostream>
#include <math.h>
#include <algorithm>
#include <limits>

// STL header files
#include <vector>
#include <queue>
#include <stack>
#include <unordered_map>
#include <unordered_set>

// DX12 header files
#include <d3d12.h>
#include <dxgi1_4.h>
#include <d3dcompiler.h>
#include <DirectXMath.h>
#include "d3dx12.h"

using namespace std;