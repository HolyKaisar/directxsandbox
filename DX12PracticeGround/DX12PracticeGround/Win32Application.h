#pragma once

#include "DXAppFrameWork.h"

class DXAppFrameWork;

class Win32Application
{
public:
	static int Run(DXAppFrameWork* pDXApp, HINSTANCE hInstance, int nCmdShow);
	static HWND GetHwnd() { return m_hwnd; }

protected:
	static LRESULT CALLBACK WindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

private:
	static HWND m_hwnd;
};